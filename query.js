const create1 = `
    CREATE DATABASE
    IF NOT EXISTS study1
    CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;
`
const user1 = `
    CREATE USER
    IF NOT EXISTS 'soomtong1'@'%'
    IDENTIFIED WITH mysql_native_password
    BY '123qwe123qwe';
`
const grant1 = `
    GRANT ALL PRIVILEGES ON study1.*
    TO 'soomtong1'@'%';
`
const show1 = `
    SELECT schema_name
    FROM information_schema.schemata;
`
const select1 = `
    SELECT
    concat(first_name, ' ', last_name)
    AS name
    FROM sakila.actor;
`
const table1 = `
    CREATE TABLE
    IF NOT EXISTS study1.member
    (
        id int primary key auto_increment,
        name varchar(100),
        email varchar(100),
        phone_number varchar(24)
    )
`
const insertMembers = `
    INSERT INTO study1.member
    (name, email, phone_number)
    VALUES ?;
`
const totalCount = `
    SELECT count(id) AS totalCount FROM study1.member
`
const selectPage = `
    SELECT * FROM study1.member ORDER BY id DESC LIMIT ?, ?
`
const members = `
    select * from
    study1.member
`
const members1 = `
    select * from
    study1.member order by name
`
const members2 = `
    select * from
    study1.member order by phone_number
`
// where named start with 'B'
const members3 = `
    select * from study1.member
    where name like 'B%'
`
// where named start with 'B' only 3
const members4 = `
    select * from study1.member
    where name like 'B%'
    limit 3
`
const actors = `
    SELECT
    actor_id
        as ID,
    concat(first_name, ' ', last_name)
        as NAME,
    length(concat(first_name, ' ', last_name))
        as NAME_LENGTH,
    substring(last_name, 1, 1)
        as FIRST_CHAR
    FROM sakila.actor;
`
const today = `
    select
        now() as today;
`
const deleteTable1 = `
    DROP TABLE study1.country;
`
const copyTable1 = `
    CREATE TABLE
    IF NOT EXISTS study1.country
    LIKE sakila.country;
`
const copyRecord1 = `
    INSERT INTO study1.country
    SELECT * FROM sakila.country;
`
module.exports = {
    create1,
    table1,
    show1,
    user1,
    grant1,
    select1,
    insertMembers,
    members,
    members1,
    members2,
    members3,
    members4,
    actors,
    today,
    deleteTable1,
    copyTable1,
    copyRecord1,
    totalCount,
    selectPage
}
