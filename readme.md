1. mysql workbench user password plugin update

```
	USE mysql;
	ALTER USER 'root'@'localhost'
	IDENTIFIED WITH mysql_native_password BY 'seoil';
```

2. upgrade ms vs code editor to new version and check auto-save feature

3. . download source from here and extract a folder named your student id
folder name: sql-study1

4. install packages and run program and
   init database,
   insert fake data 5 times

```
> npm i
> npm start
```

5. identifier naming

`camelCase` vs `snake_case`

6. mysql important functions execute with ()
	concat, substring, length, now, count
